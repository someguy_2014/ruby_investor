RSpec.describe LoansAssignmentService do

  subject(:assignment_service) {  described_class.new(loans, investor_loan_assigners) }

  let(:loans) do
    [
      Loan.new(1, 'property', 'A+', 1000),
      Loan.new(2, 'retail', 'A+', 2000),
      Loan.new(3, 'property', 'A', 3000)
    ]
  end
  let(:investor_loan_assigners) do
    [
      InvestorLoanAssigner.create(investor_name: 'Bob',   loan_filters: { category: 'property' }),
      InvestorLoanAssigner.create(investor_name: 'Helen', loan_filters: { max_amount_per_category_in_percents: { max_percents: 40, category: 'property' } }),
      InvestorLoanAssigner.create(investor_name: 'Jamie', loan_filters: [{ risk_brand_higher_than: 'B' }, { category: 'property' }])
    ]
  end
  let(:result)          { assignment_service.completed_investors.map { |investor| { investor.investor_name => investor.loans.map(&:id) } } }
  let(:expected_result) { [{ 'Bob' => [1] }, { 'Helen' => [2] }, { 'Jamie' => [3] }] }

  it 'assigns loans' do
    assignment_service.assign_loans_to_investors

    expect(result).to eq expected_result
  end
end

