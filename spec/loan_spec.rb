RSpec.describe Loan do

  subject(:loan) { described_class.new(*loan_params.values) }

  let(:loan_valid_params) { { id: 1, category_id: 'property', risk_brand_id: RiskBrand::RISK_BRAND_TO_WEIGHT_MAPPING.keys.sample, amount: 1_000} }

  context '#validate!' do

    context 'with valid risk brand id' do

      let(:loan_params) { loan_valid_params }

      it 'does not rise error' do
        expect { loan.validate! }.to_not raise_error
      end
    end

    context 'with invalid id' do

      let(:loan_params) { loan_valid_params.merge(id: nil) }

      it 'rises error' do
        expect { loan.validate! }.to raise_error StandardError
      end
    end

    context 'with invalid risk brand id' do

      let(:loan_params) { loan_valid_params.merge(risk_brand_id: 'invalid') }

      it 'rises error' do
        expect { loan.validate! }.to raise_error StandardError
      end
    end

    context 'with invalid category id' do

      let(:loan_params) { loan_valid_params.merge(category_id: 'invalid') }

      it 'rises error' do
        expect { loan.validate! }.to raise_error StandardError
      end
    end

    context 'with invalid amount' do

      let(:loan_params) { loan_valid_params.merge(amount: nil) }

      it 'rises error' do
        expect { loan.validate! }.to raise_error StandardError
      end
    end
  end
end

