RSpec.describe Category do

  subject(:category) { described_class.new(category_id) }

  context '#validate!' do

    context 'with valid category id' do

      let(:category_id) { described_class::ALLOWED.sample }

      it 'does not rise error' do
        expect { category.validate! }.to_not raise_error
      end
    end

    context 'with invalid category id' do

      let(:category_id) { 'invalid' }

      it 'rises error' do
        expect { category.validate! }.to raise_error StandardError
      end
    end
  end
end

