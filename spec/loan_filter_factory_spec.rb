RSpec.describe LoanFilterFactory do

  subject(:filter) { described_class.create(investor, loan_filter) }

  let(:investor) { Investor.new('investor name') }

  context '.create' do

    context 'when filter for allowed category' do

      let(:loan)     { Loan.new(4, category, 'A+', 100) }
      let(:category) { 'retail' }

      context 'with equal loan and filter categories' do

        let(:loan_filter) { { category: category } }

        it 'does not raise error' do
          expect { filter.call(loan) }.to_not raise_error
        end
      end

      context 'with not equal loan and filter categories' do

        let(:loan_filter) { { category: 'property' } }

        it 'raises error' do
          expect { filter.call(loan) }.to raise_error StandardError
        end
      end
    end

    context 'when filter for loan risk brand higher than' do

      let(:loan)         { Loan.new(5, 'reatil', loan_risk_brand, 100) }
      let(:loan_filter) { { risk_brand_higher_than: 'B' } }

      context 'with loan risk brand higher than the filter risk brand' do

        let(:loan_risk_brand) { 'A+' }

        it 'does not raise error' do
          expect { filter.call(loan) }.to_not raise_error
        end
      end

      context 'with loan risk brand that is lower than the filter risk brand' do

        let(:loan_risk_brand) { 'C' }

        it 'raises error' do
          expect { filter.call(loan) }.to raise_error StandardError
        end
      end

      context 'with loan risk brand that equals the filter risk brand' do

        let(:loan_risk_brand) { 'B' }

        it 'raises error' do
          expect { filter.call(loan) }.to raise_error StandardError
        end
      end
    end

    context 'when filter for max amount per category in percents' do

      let(:loan)         { Loan.new(6, 'property', 'A+', 100) }
      let(:loan_filter)  { { max_amount_per_category_in_percents: { max_percents: max_percents, category: category } } }
      let(:category)     { 'property' }
      let(:max_percents) { 50 }
      let(:investor)     { Investor.new('investor name', loans_list) }
      let(:loans_list) do
        [
          Loan.new(1, 'property', 'A+', 200),
          Loan.new(2, 'property', 'A+', 200),
          Loan.new(2, 'retail',   'A+', 500)
        ]
      end

      context 'with loan and filter in same category and the amount in percents is not exceeded' do

        it 'does not raise error' do
          expect { filter.call(loan) }.to_not raise_error
        end
      end

      context 'with loan that exceeds the amount per category in percents' do

        let(:max_percents) { 49.99 }

        it 'raises error' do
          expect { filter.call(loan) }.to raise_error StandardError
        end
      end

      context 'when assigns first loan' do

        let(:investor) { Investor.new('investor name', []) }

        it 'raises error' do
          expect { filter.call(loan) }.to raise_error StandardError
        end
      end
    end
  end
end

