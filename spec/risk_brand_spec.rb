RSpec.describe RiskBrand do

  subject(:risk_brand) { described_class.new(risk_brand_id) }

  let(:valid_risk_brand_id)   { described_class::RISK_BRAND_TO_WEIGHT_MAPPING.keys.sample }
  let(:invalid_risk_brand_id) { 'invalid risk brand id' }

  context '.risk_brand_wight_for'  do

    let(:risk_brand_wight) { described_class.risk_brand_wight_for(risk_brand_id) }

    context 'with valid risk brand id' do

      let(:risk_brand_id) { valid_risk_brand_id }

      it 'is present' do
        expect(risk_brand_wight).to_not be_nil
      end
    end

    context 'with invalid risk brand id' do

      let(:risk_brand_id) { invalid_risk_brand_id }

      it 'is present' do
        expect(risk_brand_wight).to be_nil
      end
    end
  end

  context '#validate!' do

    context 'with valid risk brand id' do

      let(:risk_brand_id) { valid_risk_brand_id }

      it 'does not rise error' do
        expect { risk_brand.validate! }.to_not raise_error
      end
    end

    context 'with invalid risk brand id' do

      let(:risk_brand_id) { invalid_risk_brand_id }

      it 'rises error' do
        expect { risk_brand.validate! }.to raise_error StandardError
      end
    end
  end
end

