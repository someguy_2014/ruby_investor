RSpec.describe InvestorLoanAssigner do

  subject(:investor_loan_assigner) { described_class.create(investor_name: investor_name, loan_filters: loan_filters) }

  let(:investor_name) { 'investor name' }
  let(:loan_filters)  { { category: category } }
  let(:loan)          { Loan.new(4, category, 'A+', 100) }
  let(:category)      { 'retail' }

  context '.create' do

    context 'with investor name and hash filter attributes' do

      it 'creates investor loan assigner' do
        expect(investor_loan_assigner).to be_a described_class
      end

      it 'has one filter' do
        expect(investor_loan_assigner.loan_filters.count).to eq 1
      end
    end

    context 'with investor name and array of hash filter attributes' do

      let(:loan_filters) { [{ risk_brand_higher_than: 'B' }, { category: category }] }

      it 'creates investor loan assigner' do
        expect(investor_loan_assigner).to be_a described_class
      end

      it 'has multiple filters' do
        expect(investor_loan_assigner.loan_filters.count).to eq 2
      end
    end
  end

  context '#assign?' do

    context 'with any filter and loan' do

      context 'when filter does not riase error' do

        it 'is true' do
          expect(investor_loan_assigner.assign?(loan)).to be true
        end

        it 'adds a loan to investor' do
          expect{
            investor_loan_assigner.assign?(loan)
          }.to change(investor_loan_assigner.investor.loans, :count).by(1)
        end
      end

      context 'when filter raises error' do

        let(:loan_filters) { { category: 'retail' } }
        let(:category)     { 'property' }

        it 'is flase' do
          expect(investor_loan_assigner.assign?(loan)).to be false
        end

        it 'does not add a loan to investor' do
          expect {
            investor_loan_assigner.assign?(loan)
          }.to_not change(investor_loan_assigner.investor.loans, :count)
        end
      end
    end
  end
end

