RSpec.describe Investor do

  subject(:investor_without_loans) { described_class.new('investor name') }
  subject(:investor_with_loans)    { described_class.new('investor name', loans_list) }

  let(:loans_list) do
    [
      Loan.new(1, 'property', 'A+', 200),
      Loan.new(2, 'property', 'A+', 200),
      Loan.new(3, 'retail', 'A+', 500)
    ]
  end
  let(:total_loans_amount) { 900.0 }

  context '#total_loans_amount' do

    context 'without loans' do

      it 'is zero' do
        expect(investor_without_loans.total_loans_amount).to be_zero
      end
    end

    context 'with loans' do

      it 'equals to total loans amount' do
        expect(investor_with_loans.total_loans_amount).to eq total_loans_amount
      end
    end
  end

  context '#amount_per_category_in_percents_for' do

    let(:new_loan_amount)                 { 100 }
    let(:loan_category_id)                { 'property' }
    let(:amount_per_category_in_percents) { 50.0 }

    context 'without loans' do

      it 'is 100%' do
        expect(investor_without_loans.amount_per_category_in_percents_for(new_loan_amount, loan_category_id)).to eq 100
      end
    end

    context 'with loans' do

      it 'equals to amount per category in percents' do
        expect(investor_with_loans.amount_per_category_in_percents_for(new_loan_amount, loan_category_id)).to eq amount_per_category_in_percents
      end
    end
  end
end

