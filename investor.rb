class Investor

  attr_reader :investor_name, :loans

  def initialize(investor_name, loans = [])
    @investor_name = investor_name
    @loans         = loans
  end

  def total_loans_amount
    loans.sum(&:amount)
  end

  def amount_per_category_in_percents_for(new_loan_amount, loan_category_id)
    total_amount_per_loan_category = loans_amount_by_category_for(loan_category_id)
    amount_per_category            = total_amount_per_loan_category + new_loan_amount
    total_amount                   = total_loans_amount + new_loan_amount

    amount_per_category / total_amount * 100
  end

  private

  def loans_amount_by_category_for(loan_category_id)
    loans.select { |loan| loan.category.category_id == loan_category_id }.sum(&:amount)
  end
end

