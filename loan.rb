class Loan

  attr_reader :id, :category, :risk_brand, :amount

  def initialize(id, category_id, risk_brand_id, amount)
    @id         = id
    @category   = Category.new(category_id)
    @risk_brand = RiskBrand.new(risk_brand_id)
    @amount     = amount.to_f
  end

  def validate!
    validate_loan_id!
    validate_loan_amount!
    risk_brand.validate!
    category.validate!
  end

  private

  def validate_loan_amount!
    raise unless amount.positive?
  end

  def validate_loan_id!
    raise if id.nil?
  end
end

