# ruby_investor

* Entity classes
  - class Category 
  - class RiskBrand
  - class Loan
  - class Investor - contains two helper methods:
    - method total_loans_amount
    - method amount_per_category_in_percents_for

* PORO classes /services, factories and mix of them/
  - class LoanFilterFactory - creates lambdas for filtering loans for investors
    - method create - as input, it takes investor object and filter params. Destructs the filter params and calls the proper private method for creating a proper lambda filter.
    - method create_allowed_category - returns lambda function for the Bob case
    - method create_risk_brand_higher_than - returns lambda function for  the Jamie case
    - method create_max_amount_per_category_in_percents - returns a lambda function for the Helen case
  - class InvestorLoanAssigner - a service class with a factory method. Basically this class is the contract between the Investor, the Loan, Filter.
    - method create - create instances of itself using predefined hash definitions, and investor name
    - method assign? - accepts a loan as a parameter
      - calls all the investor's loan filters 
      - assigns the loan to the investor /if all filters are passed successfully/
      - returns true/false, depending on, is it the loan assigned or not.
  - class LoansAssignmentService - as parameters accepts list of loans and list of InvestorLoanAssigner instances. Then assigns the loans to the investors.

