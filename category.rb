class Category

  TYPE_PROPERTY = 'property'.freeze
  TYPE_RETAIL   = 'retail'.freeze
  TYPE_MEDICAL  = 'medical'.freeze
  ALLOWED       = [TYPE_PROPERTY, TYPE_RETAIL, TYPE_MEDICAL].freeze

  attr_reader :category_id

  def initialize(category_id)
    @category_id = category_id
  end

  def validate!
    raise unless ALLOWED.include?(category_id)
  end
end

