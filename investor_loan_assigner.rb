class InvestorLoanAssigner

  def self.create(investor_name:, loan_filters:)
    investor     = Investor.new(investor_name)
    loan_filters = Array.wrap(loan_filters)

    loan_filters.map! {|loan_filter_attrs| LoanFilterFactory.create(investor, loan_filter_attrs) }

    new(investor, loan_filters)
  end

  attr_reader :investor, :loan_filters

  def initialize(investor, loan_filters)
    @investor    = investor
    @loan_filters = loan_filters
  end

  def assign?(loan)
    loan_filters.each { |loan_filter| loan_filter.call(loan) }
    investor.loans << loan
    true
  rescue
    false
  end

end

