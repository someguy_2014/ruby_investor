class RiskBrand

  RISK_BRAND_TO_WEIGHT_MAPPING = {
    'A+' => 6,
    'A'  => 5,
    'B'  => 4,
    'C'  => 3,
    'D'  => 2,
    'E'  => 1,
  }.freeze

  def self.risk_brand_wight_for(risk_brand_id)
    RISK_BRAND_TO_WEIGHT_MAPPING[risk_brand_id]
  end

  attr_reader :risk_brand_id, :weight

  def initialize(risk_brand_id)
    @risk_brand_id = risk_brand_id
    @weight        = self.class.risk_brand_wight_for(risk_brand_id)
  end

  def validate!
    raise unless RISK_BRAND_TO_WEIGHT_MAPPING.key?(risk_brand_id)
  end
end

