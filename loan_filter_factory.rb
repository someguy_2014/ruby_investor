class LoanFilterFactory

  FILTER_NAME_TO_CREATE_METHOD_MAPPING = {
    category:                            :create_allowed_category,
    risk_brand_higher_than:              :create_risk_brand_higher_than,
    max_amount_per_category_in_percents: :create_max_amount_per_category_in_percents,
  }

  attr_reader :investor

  def self.create(investor, loan_filter_attrs)
    filter_name, filter_args            = *loan_filter_attrs.first
    investor_filter_factory             = new(investor)
    investor_filter_factory_method_name = FILTER_NAME_TO_CREATE_METHOD_MAPPING[filter_name]

    investor_filter_factory.public_send(investor_filter_factory_method_name, filter_args)
  end

  def initialize(investor)
    @investor = investor
  end

  def create_allowed_category(loan_category_id)
    ->(loan) { raise unless loan.category.category_id == loan_category_id }
  end

  def create_risk_brand_higher_than(risk_brand_id)
    risk_brand_weight = RiskBrand.risk_brand_wight_for(risk_brand_id)

    ->(loan) { raise unless loan.risk_brand.weight > risk_brand_weight }
  end

  def create_max_amount_per_category_in_percents(filter_args)
    loan_category_id                    = filter_args[:category]
    max_amount_per_category_in_percents = filter_args[:max_percents]

    ->(loan) {
      return unless loan_category_id == loan.category.category_id

      amount_per_category_in_percents = investor.amount_per_category_in_percents_for(loan.amount, loan_category_id)

      raise if amount_per_category_in_percents > max_amount_per_category_in_percents
    }
  end

  private_class_method :new
end

