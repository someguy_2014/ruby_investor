class LoansAssignmentService

  attr_reader :loans, :investor_loan_assigners, :completed_investors, :fair_assignment_count

  def initialize(loans = [], investor_loan_assigners = [])
    @loans                   = loans
    @investor_loan_assigners = investor_loan_assigners
    @completed_investors     = []
  end

  def assign_loans_to_investors
    return if loans.empty? || investor_loan_assigners.empty?

    @fair_assignment_count = loans.count.to_f / investor_loan_assigners.count.to_f
    while loans.size > 0 && investor_loan_assigners.size > 0
      loan                   = loans.shift
      investor_loan_assigner = investor_loan_assigners.detect do |investor_loan_assigner|
        investor_loan_assigner.assign?(loan)
      end

      next if investor_loan_assigner.nil?

      assigned_loan_to_investor_for(investor_loan_assigner)
    end
    completed_investors.concat(investor_loan_assigners.map(&:investor))
  end

  private

  def assigned_loan_to_investor_for(investor_loan_assigner)
    investor = investor_loan_assigner.investor
    if investor.loans.count >= fair_assignment_count
      completed_investors.push(investor_loan_assigner.investor)
      investor_loan_assigners.delete(investor_loan_assigner)
    else
      investor_loan_assigners.push(investor_loan_assigner)
    end
  end
end

